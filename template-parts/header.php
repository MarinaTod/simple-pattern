<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width" initial-scale="1.0">
    <link rel="stylesheet" href="../main.min.css">
    <title>Title</title>

    <script src="../main.min.js"></script>
</head>
<body>
<div class="body-bg">
    <header class="main-header">

        <div class="bg-top-triangle">
            <div class="container row -nav">
                <div class="logo">

                </div>

                <nav class="main-menu" onclick="showMenu()">
                    <div class="mobile-menu-part"></div>
                    <ul class="items-container">
                        <li class="menu-item">page1</li>
                        <li class="menu-item">page2</li>
                        <li class="menu-item">page3</li>
                        <li class="menu-item">page4</li>
                    </ul>
                </nav>
            </div>
        </div>

        <div class="container row -heading">
            <h1 class="title">Lorem ipsum dolor sit amet consect</h1>
        </div>
    </header>