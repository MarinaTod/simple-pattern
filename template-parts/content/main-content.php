<section class="container about">
    <h2 class="heading">
        Lorem ipsum
    </h2>

    <p class="info">
        Lorem ipsum dolor sit amet,
        consectetur adipisicing elit. Assumenda
        cumque cupiditate dicta ex exercitationem
        facere fugiat impedit iste laudantium magni
        mollitia natus non odio.
    </p>

    <div class="goto">
        More
    </div>
</section>
</div>

<section class="works">
    <h2 class="heading">
        Lorem ipsum
    </h2>


    <div class="gallery -demo">
        <div class="container">
            <div class="item">
                <img src="../../assets/public/images/gallery-item1.jpg" alt="">
            </div>
            <div class="item">
                <img src="../../assets/public/images/gallery-item2.jpg" alt="">
            </div>
        </div>

        <div class="goto">
            More
        </div>
    </div>
</section>
</main>
