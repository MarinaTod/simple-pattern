# SimplePattern
Reference on main page
http://marinatod.esy.es/MyPortfolio/index.php

Simple cross-browser web-site(for modern web-browsers) with simple for understanding structure of project.
Technologies and other instruments is used in this project:
1. Html5/css3: 
  
    * sass 
  
    * flexbox
    
    * animation 
  
    * understandable html 5 tags for all blocks and elements;
2. Methodology RCSS(more info about methodology: http://rscss.io/) for better web-codding;
3. js for dynamic content and parallax effects;
4. gulp for compiling, cleaning and minifing sass and for cleaning, minifing, uglifing(renaming variables, functions) js;
5. npm for installing third-party libraries and gulp;
6. progressive jpegs.
