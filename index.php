<?php

require_once 'template-parts/header.php';

?>
    <main class="main-content">
        <div class="bg-bottom-triangle">
<?php

require_once 'template-parts/content/main-content.php';

require_once 'template-parts/footer.php';